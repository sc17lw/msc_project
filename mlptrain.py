# -*- coding: utf-8 -*-
"""
Created on Sat May 29 15:00:57 2021

@author: GS65
"""
import pandas as pd
import torch
from torch.utils.data import DataLoader, TensorDataset
from model import MLPModel
from torch import nn, optim
import matplotlib.pyplot as plt
import numpy as np

df = pd.read_csv('mlpresult.csv')

# df[[]] multiple target
# df. values returns a numpy array with the underlying data of the DataFrame, 
# without any index or columns names
inputs = df[['force_x','force_y','fp_x','fp_y']].values
targets = df[['position_x','position_y','rotation']].values
print(inputs.shape)


train_split = 0.8
train_size = int(len(targets)*train_split)

# =============================================================================
# print(train_size)
# train_data = dataset[:train_size]
# print(train_data[0])
# print(len(train_data))
# =============================================================================

# =============================================================================
# inputs = inputs[:train_size]
# targets = targets[:train_size]
# =============================================================================
trainset = TensorDataset(torch.tensor(inputs[:train_size], dtype = torch.float32), torch.tensor(targets[:train_size], dtype = torch.float32))
testset = TensorDataset(torch.tensor(inputs[train_size:], dtype = torch.float32), torch.tensor(targets[train_size:], dtype = torch.float32))

train_loader = DataLoader(
    trainset,
    batch_size= 64,
    shuffle = True
    )

test_loader = DataLoader(
    testset,
    batch_size= 64,
    shuffle = False
    )

model = MLPModel()
loss_fn = nn.MSELoss()
# =============================================================================
# optimizer = optim.SGD(model.parameters(), 0.01)
# =============================================================================
optimizer = optim.Adam(model.parameters(), lr=0.01)
nepochs = 20
losses = np.zeros(nepochs)
for epoch in range(nepochs):
    running_loss = 0
    n = 0
    for data in train_loader:
        input, target = data
        
        output = model(input)
        loss = loss_fn(output, target)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        #item() moves the data to CPU. It converts the value into a plain python number. 
        #And plain python number can only live on the CPU.
        running_loss+=loss.item()       
        n+=1
        losses[epoch] = running_loss/n
    print(f"epoch: {epoch} loss: {running_loss/n}")
 
#Torch. no_grad() deactivates autograd engine. 
#Eventually it will reduce the memory usage and speed up computations. 
plt.plot(losses,'r',label = 'training_loss',)

plt.legend()
plt.xlabel('epoch')
plt.ylabel('loss')   
with torch.no_grad():
    running_loss =0  
    n=0
    test_losses =0
    for data in test_loader:
        input, target = data
        output = model(input)
        loss = loss_fn(output, target)
        running_loss += loss.item()
        n+=1
        test_losses += running_loss/n
    print(test_losses)
        
# =============================================================================
# print(running_loss/len(test_loader))
# =============================================================================
# =============================================================================
# with torch.no_grad():     
# =============================================================================
# =============================================================================
#     print(testset[600])      
#     input, target = testset[600]
#     print(target)
#     print(model(input))        
#     
# =============================================================================



        
        
        
        
        
        
        
        
        
        