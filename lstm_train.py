# -*- coding: utf-8 -*-
"""
Created on Sat Jul 10 15:33:24 2021

@author: GS65
"""
from lstm_utils import gen_sequence, gen_labels
import pandas as pd
import numpy as np
from model import LSTM1
import torch
from torch.utils.data import DataLoader, TensorDataset
from torch.autograd import Variable
from torch import nn, optim
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")



train_df = pd.read_csv('lstm_result.csv')

# =============================================================================
# sequence_cols = ['sample_id','v_x','v_y','w_z','position_x','position_y','position_z','rotation']
# =============================================================================


scaler = MinMaxScaler(feature_range=(-1, 1))


cols_normalize = train_df.columns.difference(['sample_id','timestep'])
norm_train_df = pd.DataFrame(scaler.fit_transform(train_df[cols_normalize]), 
                             columns=cols_normalize, 
                             index=train_df.index)
join_df = train_df[train_df.columns.difference(cols_normalize)].join(norm_train_df)
train_df = join_df.reindex(columns = train_df.columns)

sequence_length = 15
# generate sequence
sequence_gen = []
sequence_cols = ['v_x','v_y','w_z']
for id in train_df['sample_id'].unique():
    val = gen_sequence(train_df[train_df['sample_id']==id], sequence_length, sequence_cols)
    if len(val) >0:
        sequence_gen.append(val)

# generate sequences and convert to numpy array
seq_array = np.concatenate(sequence_gen).astype(np.float32)

label_gen = []
for id in train_df['sample_id'].unique():
    val = gen_labels(train_df[train_df['sample_id']==id], sequence_length, ['position_x','position_y','position_z','rotation'])
    if len(val) >0:
        label_gen.append(val)
# =============================================================================
# label_gen = [gen_labels(train_df[train_df['sample_id']==id], sequence_length, ['position_x','position_y','position_z','rotation']) 
#              for id in train_df['sample_id'].unique()]
# =============================================================================
label_array = np.concatenate(label_gen).astype(np.float32)
print(seq_array.shape)
print(label_array.shape)

# pytorch tensors
train_size = int(len(label_array) * 0.8)
train_X = Variable(torch.tensor(seq_array[0:train_size]))
train_Y = Variable(torch.tensor(label_array[0:train_size]))
test_X = Variable(torch.tensor(seq_array[train_size:len(label_array)]))
test_Y = Variable(torch.tensor(label_array[train_size:len(label_array)]))

trainset = TensorDataset(train_X, train_Y)
testset = TensorDataset(test_X, test_Y)
print(len(trainset))
train_loader = DataLoader(
    trainset,
    batch_size= 64,
    shuffle = False
    )

test_loader = DataLoader(
    testset,
    batch_size= 64,
    shuffle = False
    )

#####  Parameters  ######################
num_epochs = 30
learning_rate = 1e-3

#####Init the Model #######################
# =============================================================================
# lstm = LSTM(num_classes, input_size, hidden_size, num_layers)
# =============================================================================
num_classes = 4
input_size = 3
hidden_size = 128
num_layers = 1
lstm = LSTM1(num_classes, input_size, hidden_size, num_layers).to(device)

##### Set Criterion Optimzer and scheduler ####################
loss_fn = nn.MSELoss().to(device)    # mean-squared error for regression
optimizer = optim.Adam(lstm.parameters(), lr=learning_rate)
scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer,  patience=500,factor =0.5 ,min_lr=1e-7, eps=1e-08)
#optimizer = torch.optim.SGD(lstm.parameters(), lr=learning_rate)

# Train the model
losses = np.zeros(num_epochs)

for epoch in range(num_epochs): 
    running_loss = 0
    n = 0
    for data in train_loader:
        input, target = data
        input = input.to(device)
        target = target.to(device)
        optimizer.zero_grad()

        
        output = lstm(input)

        loss = loss_fn(output, target)
        
        loss.backward()
        optimizer.step()
        running_loss+=loss.item()       
        n+=1
        losses[epoch] = running_loss/n
    print(f"epoch: {epoch} loss: {running_loss/n}")
        #item() moves the data to CPU. It converts the value into a plain python number. 
        #And plain python number can only live on the CPU.
plt.plot(losses,'r',label = 'training_loss',)

plt.legend()
plt.xlabel('epoch')
plt.ylabel('loss')  
# =============================================================================
# for epoch in range(num_epochs): 
# 
#     lstm.train()
#     outputs = lstm(train_X.to(device))
#     optimizer.zero_grad()
#     
#     # obtain the loss function
#     loss = loss_fn(outputs, train_Y.to(device))
#     
#     loss.backward()   
#     optimizer.step()
# 
#     print("Epoch:%d, loss:%1.5f" %(epoch, loss))
# =============================================================================
    
# =============================================================================
# with torch.no_grad():
#     valid = lstm(test_X.to(device))
#     valid_loss = loss_fn(valid, test_Y.to(device))
#     print(valid_loss)
#     
#     v = lstm(test_X[:1].to(device)) 
#     print(test_X[:1])
#     print(v)
# =============================================================================
    
    
    
    
    
    
    
    