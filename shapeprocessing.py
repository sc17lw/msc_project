# -*- coding: utf-8 -*-
"""
Created on Wed Jul 28 14:25:46 2021

@author: GS65
"""
import provider
import trimesh
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
import torch
import torchvision.transforms as transforms
from pointnet2_cls_ssg import get_model
from torch.autograd import Variable
net = get_model(40, False)
checkpoint=torch.load('pointnet2_ssg_wo_normals/checkpoints/best_model.pth')
net.load_state_dict(checkpoint['model_state_dict'])
objmesh = 'myurdf/cube.obj'

mesh = trimesh.load(objmesh)

points = mesh.sample(1024)
points = torch.Tensor(points)
points = provider.random_point_dropout(points)
points[:, :, 0:3] = provider.random_scale_point_cloud(points[:, :, 0:3])
points[:, :, 0:3] = provider.shift_point_cloud(points[:, :, 0:3])
points = points.transpose(2,1)
pred, trans_feat = net(points)
# =============================================================================
# fig = plt.figure(figsize=(5, 5))
# ax = fig.add_subplot(111, projection="3d")
# ax.scatter(points[:, 0], points[:, 1], points[:, 2],color = 'red')
# ax.set_axis_off()
# plt.show()
# =============================================================================
# =============================================================================
# print(points)
# cloud=trimesh.PointCloud(mesh.sample(100))
# print(cloud)
# cloud.export('demo.ply')
# 
# # =============================================================================
# # transf = transforms.ToTensor()
# # point_tensor = transf(np.float32(points))
# # =============================================================================
# t = Variable(torch.tensor(points))
# batch_t = torch.unsqueeze(t, 0)
# # =============================================================================
# # print(point_tensor.shape)
# # =============================================================================
# 
# outputs = net(batch_t)
# _, predicted = torch.max(outputs.data, 1)
# print(predicted)
# =============================================================================

# function to parse dataset
# =============================================================================
# def parse_dataset(num_points=2048):
#  
#     train_points = []
#     train_labels = []
#     test_points = []
#     test_labels = []
#     class_map = {}
#     folders = glob.glob(os.path.join(DATA_DIR, "[!README]*"))
#  
#     for i, folder in enumerate(folders):
#         print("processing class: {}".format(os.path.basename(folder)))
#         # store folder name with ID so we can retrieve later
#         class_map[i] = folder.split("/")[-1]
#         # gather all files
#         train_files = glob.glob(os.path.join(folder, "train/*"))
#         test_files = glob.glob(os.path.join(folder, "test/*"))
#  
#         for f in train_files:
#             train_points.append(trimesh.load(f).sample(num_points))
#             train_labels.append(i)
#  
#         for f in test_files:
#             test_points.append(trimesh.load(f).sample(num_points))
#             test_labels.append(i)
#  
#     return (
#         np.array(train_points),
#         np.array(test_points),
#         np.array(train_labels),
#         np.array(test_labels),
#         class_map,
#     )
# =============================================================================
