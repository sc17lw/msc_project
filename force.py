from model import MLPModel

import numpy as np

a = np.arange(20).reshape(2,5,2)
print(a)
p = []
p.append(a)
print(p)
c = np.concatenate(p)
print('*'*10)
print(c)
print('*'*10)
print(c[:2])
