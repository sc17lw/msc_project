# MSC_project



## Getting started
We use python3 and anaconda for this project. \
All simulated data can be downloaded from http://download.cs.stanford.edu/orion/predicting_physical_dynamics/SimData.zip (1 GB). \
Pre-trained PointNet weights that we use to initialize training can be downloaded http://download.cs.stanford.edu/orion/predicting_physical_dynamics/pretrained_pointnet.zip (43 MB)

## Install Pybullet
You have to install pybullet first to collect data:\
To install this package with conda run:\
conda install -c conda-forge pybullet

## Run Simulation
This will run the simulation for the mlp baseline:\
python3 mlpcollect.py \
This will run the simulation for the lstm:\
python3 lstmcollect.py 

## Run Training
This will run the training process for the mlp baseline:\
python3 mlptrain.py \
This will run the training process for the lstm:\
python3 lstmtrain.py 


***


