# -*- coding: utf-8 -*-
"""
Created on Wed May 26 19:58:51 2021

@author: GS65
"""
import os
import pybullet as p
import pybullet_data
import time
import numpy as np
import pandas as pd
import math

# =============================================================================
# df = pd.DataFrame(columns=('force vector','linear velocity','angular velocity','position','rotation'))
# =============================================================================


# physics engine
# =============================================================================
# p.connect(p.DIRECT)
# =============================================================================
p.connect(p.GUI)
p.setAdditionalSearchPath(pybullet_data.getDataPath())

# gravity
p.setGravity(0, 0, -9.8)

# load plane
groundID = p.loadURDF("plane.urdf")

# In[2]
cubeID = p.loadURDF("myurdf/cube1.urdf",basePosition = [0,0,0.5])

cubePos0, cubeOrn0 = p.getBasePositionAndOrientation(cubeID)


def generateRandomPoint(x,y):
    while True:
        x = np.random.uniform (x-20,x+20)
        y = np.random.uniform (y-20,y+20)
# =============================================================================
#         if x*x + y*y >= 10*10 and x*x + y*y <=20*20:
# =============================================================================
        if x*x + y*y > 100:
            return x,y
# =============================================================================
# P_min, P_max = p.getAABB(cubeID)
# =============================================================================
# =============================================================================
# px= cubePos0[0]
# py = cubePos0[1]
# x,y=generateRandomPoint(px,py)
# rayFrom =[x,y,cubePos0[2]]
# rayTo = cubePos0
# result = p.rayTest(rayFrom,rayTo)
# print(result[0])
# =============================================================================

for i in range(480):
    p.stepSimulation()
    
# =============================================================================
# p.resetBaseVelocity(cubeID,[0,0,0],[0,0,0])
# =============================================================================
def getForcePos():
    px= cubePos0[0]
    py = cubePos0[1]
    x,y=generateRandomPoint(px,py)
    rayFrom =[x,y,cubePos0[2]]
    rayTo = cubePos0
    rayInfo = p.rayTest(rayFrom,rayTo)
    
    return rayInfo

# In[3]

def apply_push():
    datalist = []
    p.resetBasePositionAndOrientation(cubeID, [0, 0, 1],p.getQuaternionFromEuler([0, 0, 0]))
    total_rotation = 0
    DURATION = 60
    #cube pos
    
    #force
    forcex = np.random.uniform (0,20)
    forcey = np.random.uniform (0,20)

    while True:
        Info = getForcePos()
        if Info[0][0] ==cubeID:
            rayInfo = Info
            break

      
    #force position
    pos = list(rayInfo[0][3])
    pos_x = pos[0]
    pos_y = pos[1]
# =============================================================================
#     datalist.append([forcex,forcey])
# =============================================================================
    for i in range(DURATION):      
        cubePos_start, cubeOrn_start = p.getBasePositionAndOrientation(cubeID)
           
# =============================================================================
#         pos = [cubePos[0]+pos_x, cubePos[1]+pos_y, cubePos[2]]
# =============================================================================
        p.applyExternalForce(objectUniqueId=cubeID, linkIndex=-1,forceObj=[forcex,forcey,0], posObj=[pos_x,pos_y,0], flags=p.LINK_FRAME)
        p.stepSimulation()
        cubePos_end, cubeOrn_end = p.getBasePositionAndOrientation(cubeID)
        rotation = p.getDifferenceQuaternion(cubeOrn_start,cubeOrn_end)
        rotation = p.getEulerFromQuaternion(rotation)
        total_rotation += rotation[2]
        v,wv = p.getBaseVelocity(cubeID)
    # =============================================================================
    #     time.sleep(1./240.)
    # =============================================================================
# =============================================================================
#         if i == 0:           
#             datalist.append(v)
#             datalist.append(wv)
# =============================================================================

    
    for i in range(1200):
        cubePos_start, cubeOrn_start = p.getBasePositionAndOrientation(cubeID)
        v,wv = p.getBaseVelocity(cubeID)
# =============================================================================
#         angle = p.getEulerFromQuaternion(rotation)       
# =============================================================================
        p.stepSimulation()
# =============================================================================
#         time.sleep(1./240)
# =============================================================================
        cubePos_end, cubeOrn_end = p.getBasePositionAndOrientation(cubeID)
        rotation = p.getDifferenceQuaternion(cubeOrn_start,cubeOrn_end)
        rotation = p.getEulerFromQuaternion(rotation)
        total_rotation += rotation[2]
        if np.linalg.norm(v)<0.001 and np.linalg.norm(wv)<0.001 and i>1000: 
            #final pos
            cubePos, cubeOrn = p.getBasePositionAndOrientation(cubeID)
            datalist.append(cubePos)
            datalist.append(total_rotation)
            break

    

# =============================================================================
#     if len(datalist) ==5:
#         forcevector = datalist[0]
#         force_x,force_y = forcevector[0],forcevector[1]
#         vx,vy,vz = datalist[1][0],datalist[1][1],datalist[1][2],
#         wx,wy,wz = datalist[2][0],datalist[2][1],datalist[2][2],
#         px,py,pz = datalist[3][0],datalist[3][1],datalist[3][2],
#         rotation = total_rotation
# =============================================================================
# =============================================================================
#     result = {'force_x': force_x,
#                    'force_y': force_y,
#                    'v_x':vx,
#                    'v_y':vy,
#                    'v_z':vz,
#                    'w_x':wx,
#                    'w_y':wy,
#                    'w_z':wz,
#                    'position_x':px,
#                    'position_y':py,
#                    'position_z':pz,
#                    'rotation': rotation}
# =============================================================================
    position_x = datalist[0][0]
    position_y = datalist[0][1]
    rotation = datalist[1]

    result = {'force_x': forcex,
                   'force_y': forcey,
                   'fp_x': pos_x,
                   'fp_y':pos_y,
                   'position_x': position_x,
                   'position_y':position_y,
                   'rotation': rotation}
    return result
# =============================================================================
#         print(force_x)
# =============================================================================
        

df1 = pd.DataFrame(columns=('force_x','force_y','fp_x','fp_y','position_x','position_y','rotation'))     
df = pd.DataFrame(columns=('v_x','v_y','v_z','w_x','w_y','w_z','position_x','position_y','position_z','rotation'))
        
for n in range(8000):
    if n ==1000:
        print(1)
    result = apply_push()
    
    df1.loc[n] = result
# =============================================================================
#     df = df.append(result,ignore_index=True)
# =============================================================================
      
df1.to_csv('mlpresult.csv',index=False)         
