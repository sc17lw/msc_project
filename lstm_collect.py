# -*- coding: utf-8 -*-
"""
Created on Wed May 26 19:58:51 2021

@author: GS65
"""
import os
import pybullet as p
import pybullet_data
import time
import numpy as np
import pandas as pd
import math

df = pd.DataFrame(columns=('sample_id','timestep','v_x','v_y','w_z','position_x','position_y','position_z','rotation','v'))
        
# physics engine
p.connect(p.DIRECT)
# =============================================================================
# p.connect(p.GUI)
# =============================================================================
p.setAdditionalSearchPath(pybullet_data.getDataPath())

# gravity
p.setGravity(0, 0, -9.8)

# load plane
groundID = p.loadURDF("plane.urdf")

# In[2]
cubeID = p.loadURDF("myurdf/cube1.urdf",basePosition = [0,0,0.5])

cubePos0, cubeOrn0 = p.getBasePositionAndOrientation(cubeID)
#
def generateRandomPoint(x,y):
    while True:
        x = np.random.uniform (x-20,x+20)
        y = np.random.uniform (y-20,y+20)
        if x*x + y*y > 100:
            return x,y


for i in range(480):
    p.stepSimulation()
# =============================================================================
#     time.sleep(1./240.)
# =============================================================================

def getForcePos():
    px= cubePos0[0]
    py = cubePos0[1]
    x,y=generateRandomPoint(px,py)
    rayFrom =[x,y,cubePos0[2]]
    rayTo = cubePos0
    rayInfo = p.rayTest(rayFrom,rayTo)
    
    return rayInfo

# In[3]

def apply_push(sample_id):
    datalist = []
    p.resetBasePositionAndOrientation(cubeID, [0, 0, 0.5],p.getQuaternionFromEuler([0, 0, 0]))
    total_rotation = 0
    DURATION = 60
    step = 0
    
    #cube pos
    
    #force
    forcex = np.random.uniform (0,100)
    forcey = np.random.uniform (0,100)

    while True:
        Info = getForcePos()
        if Info[0][0] ==cubeID:
            rayInfo = Info
            break

      
    #force position
    pos = list(rayInfo[0][3])
    pos_x = pos[0]
    pos_y = pos[1]
# =============================================================================
#     datalist.append([forcex,forcey])
# =============================================================================
    for i in range(DURATION):      
        cubePos_start, cubeOrn_start = p.getBasePositionAndOrientation(cubeID)
           
        p.applyExternalForce(objectUniqueId=cubeID, linkIndex=-1,forceObj=[forcex,forcey,0], posObj=[pos_x,pos_y,0], flags=p.LINK_FRAME)
        p.stepSimulation()
        cubePos_end, cubeOrn_end = p.getBasePositionAndOrientation(cubeID)
        rotation = p.getDifferenceQuaternion(cubeOrn_start,cubeOrn_end)
        rotation = p.getEulerFromQuaternion(rotation)
        total_rotation += rotation[2]
        
        #collect data every 15hz
        if step%15 == 0:
            v,wv = p.getBaseVelocity(cubeID)
            vx = v[0]
            vy = v[1]
            vz = v[2]
            wx = wv[0]
            wy = wv[1]
            wz = wv[2]
            px = cubePos_end[0]
            py = cubePos_end[1]
            pz = cubePos_end[2]
            rz = rotation[2]
            result1 = {
                'sample_id':sample_id,
                'timestep': step,
                'v_x':vx,
                'v_y':vy,
                'w_z':wz,
                'position_x':px,
                'position_y':py,
                'position_z':pz,
                'rotation': rz,
                'v':vx*vx + vy*vy
                }
            datalist.append(result1)
           
        step+=1

       
    for i in range(1200):
        cubePos_start, cubeOrn_start = p.getBasePositionAndOrientation(cubeID)
        v,wv = p.getBaseVelocity(cubeID)
        p.stepSimulation()
        cubePos_end, cubeOrn_end = p.getBasePositionAndOrientation(cubeID)
        rotation = p.getDifferenceQuaternion(cubeOrn_start,cubeOrn_end)
        rotation = p.getEulerFromQuaternion(rotation)
        if step%15 == 0:
            v,wv = p.getBaseVelocity(cubeID)
            vx = v[0]
            vy = v[1]
            vz = v[2]
            wx = wv[0]
            wy = wv[1]
            wz = wv[2]
            px = cubePos_end[0]
            py = cubePos_end[1]
            pz = cubePos_end[2]
            rz = rotation[2]
            result1 = {
                'sample_id': sample_id,
                'timestep': step,
                'v_x':vx,
                'v_y':vy,
                'w_z':wz,
                'position_x':px,
                'position_y':py,
                'position_z':pz,
                'rotation': rz,
                'v':vx*vx + vy*vy
                }
            datalist.append(result1)           
        step+=1
# =============================================================================
#         cubePos_end, cubeOrn_end = p.getBasePositionAndOrientation(cubeID)
# =============================================================================
# =============================================================================
#         rotation = p.getDifferenceQuaternion(cubeOrn_start,cubeOrn_end)
#         rotation = p.getEulerFromQuaternion(rotation)
#         total_rotation += rotation[2]
# =============================================================================
        if np.linalg.norm(v)<0.001 and np.linalg.norm(wz)<0.001: 
            #final pos
            cubePos, cubeOrn = p.getBasePositionAndOrientation(cubeID)
            break
                     
# =============================================================================
#             datalist.append(cubePos)
#             datalist.append(total_rotation)
# =============================================================================
            
        
# =============================================================================
#         time.sleep(1./240.)
# =============================================================================

    
# =============================================================================
#     position_x = datalist[0][0]
#     position_y = datalist[0][1]
#     rotation = datalist[1]
# 
#     result = {'force_x': forcex,
#                    'force_y': forcey,
#                    'fp_x': pos_x,
#                    'fp_y':pos_y,
#                    'position_x': position_x,
#                    'position_y':position_y,
#                    'rotation': rotation}
# =============================================================================
    #linear and angular

    return datalist
    
# =============================================================================
#         print(force_x)
# =============================================================================
        

df1 = pd.DataFrame(columns=('force_x','force_y','fp_x','fp_y','position_x','position_y','rotation'))     


if __name__ == '__main__':
    index = 0
    sample_id = 0
    for n in range(500):
        if n ==40:
            print(1)
    # =============================================================================
    #     result = apply_push()
    # =============================================================================
        sample_id+=1
        data = apply_push(sample_id)
        for row in data:
            df.loc[index] = row
            index+=1
        
    df.to_csv('lstm_result1.csv', index = False) 
    

      
